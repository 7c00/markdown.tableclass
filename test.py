#!/usr/bin/env python
#coding:utf-8

import markdown

md = u'''
# students

number|name|score  
--|--|--
1|Tom|6
2|Jerry|7

'''

html = markdown.markdown(md, ['tables', "tableclass.tableclass"])
print(html)
