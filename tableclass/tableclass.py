#!/usr/bin/env python

from markdown.treeprocessors import Treeprocessor
from markdown.extensions import Extension

class TableClassTreeProcessor(Treeprocessor):

    def run(self, doc):
        for elem in doc.getiterator():
            if elem.tag in ['table']:
                elem.set('class', 'table table-bordered')

class TableClassExtention(Extension):

    def extendMarkdown(self, md, md_globals):
        md.treeprocessors.add('tableclass', TableClassTreeProcessor(md), "_end")

def makeExtension(configs={}):
    return TableClassExtention(configs=configs)
